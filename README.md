![pipeline](https://gitlab.com/GrandChaman/fuzt/badges/master/build.svg "Build status")
![coverage](https://gitlab.com/GrandChaman/fuzt/badges/master/coverage.svg "Code coverage")

# Fuzt

Fuzt is a testing library for `C` projects. Its purpose is to help one to find
edge cases problems whilst making use of system calls.

It allows one to be more confident in one's code even in the harshest runtime
environmnent.

## Features

- Configuration via environmnent variables.
- Make every system calls fail after a certains number of calls.
- Make every system call fail in separates process.
- Quit after making a system call fail.
- Display the number and location of all system calls.
- Replay the program _n_ times making a different system call fail each time.

## How it works

The library will hook onto the system calls. After the condition for failure is
met, the function will repeatedly fails. If the condition isn't met yet or if
the project is not enable (see options below), the real system call will be called.

Your code should already work _as is_.

## Supported functions

| Name  | Fail value  |
|---|---|
| `malloc`  | `NULL`  |
| `realloc`  |  `NULL` |
| `calloc`  | `NULL`  |
|  `mmap` | `NULL`  |
| `munmap`  |-1   |
| `open`  |  -1 |
| `write`  | -1  |
| `fstat` |  -1 |

## Getting started

### Building

One can build the project using : 

```bash
make # Simple build
make install # Install in /usr/local/lib and /usr/local/include
```

### Testing

One can run the project tests using : 

```bash
make test
./__tests/run.sh
```

### Usage

You should import the `fuzt/setup.h` include in all the file that make use of
system calls. When compiling, don't forget to link against the static library
`libfuzt.a` (`-lfuzt`)

| Variable name  | Description  |
|---|---|
| `FUZT_ENABLE`  | Will enable the features  |
| `FUZT_NB_CALL`  | The number of call before failing in fixed mode  |
| `FUZT_FIXED`  | Enable the fixed mode |
| `FUZT_SEQ_REPLAY`  | Replay each system call in a different process making it fail  |
| `FUZT_DYNAMIC`  | Enable the dynamic mode (priority over fixed mode) |
| `FUZT_PRINT_LIST`  | Print the list of system calls  |
| `FUZT_STOP_ON_CALL`  | After having made a system call fail. Exit on the next system call  |
| `FUZT_COLOR`  | Allow colors in the output  |

## Examples

All the following examples will be based on this code :

```C
#include <stdlib.h>
#include <fuzt/setup.h>

int main()
{
	(void)malloc(1);

	if ((write(1, "A simple message\n", 17)) < 0)
		abort();

	size_t *foo = (size_t*)malloc(sizeof(size_t));
	if (!foo)
		return (1);
	*foo = 0;
	free(foo);
	size_t *bar = (size_t*)malloc(sizeof(size_t));
	*bar = 6;
	return (0);
}
```

The output for the dynamic and fixed mode is as follow :

```
[PID][+call number]		file_name:line_number		<function_name>: Signal name (signal number)
```

### Print list

This features allows one to list the name and positions of system calls :



![Print List](examples/print_list.png "Print List")

### Fixed call

This features allows one to make the main process' system calls after a certains
number of calls. 

In the first image the system calls fail from the first one.

![Fixed Calls 0](examples/fixed_call_0.png "Fixed Calls 0")

The second picture allow 2 system calls to execute correctly.

![Fixed Calls 2](examples/fixed_call_2.png "Fixed Calls 2")

### Sequential replay

The sequential replay will let the program ends then, for each system calls made,
it'll create a child process which will fail from that system calls,
display the result at the end.

![Sequential](examples/fixed_seq_replay.png "Sequential")

### Dynamic

The dynamic mode will fork each time a system call is made. The main process
will wait for the child process to finish. Result will not be display before the
end of the main process. The child process output will be silenced. If the
`FUZT_STOP_ON_CALL` option is set, the child process will be exitted normally if
this one survived the first failed system call, allowing for a quicker execution.

![Dynamic](examples/dynamic_stop_on_call.png "Dynamic")

