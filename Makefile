# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/16 13:28:08 by fle-roy           #+#    #+#              #
#    Updated: 2019/10/07 18:42:22 by fle-roy          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

vpath %.c src
vpath %.c src/llist
vpath %.c __tests

SRC = \
	fuzt.c \
	llist_del.c \
	llist_destroy.c \
	llist_new.c \
	llist_push_back.c \
	llist_push_front.c \
	utils.c \
	process.c
INCLUDE= include
OBJ_DIR=obj
DEP_DIR=dep
VERSION_DEFINE := $(shell git describe master --tags)-$(shell \
	git rev-parse --short HEAD)
CFLAGS = -g3 -Wall -Wextra -Werror -I $(INCLUDE) -I malloc/include \
	-DVERSION="\"$(VERSION_DEFINE)\""
LFLAGS = rsc
CC = cc
LN = ar
TEST_CFLAGS = -I $(INCLUDE)
TEST_LDFLAGS = -lcriterion
TEST_BIN = fuzt_test
TEST_SRC = llist.test.c
OBJ = $(SRC:%.c=$(OBJ_DIR)/%.o)
DEP = $(SRC:%.c=$(DEP_DIR)/%.d)
OBJ_TEST = $(TEST_SRC:%.test.c=$(OBJ_DIR)/%.test.o)
DEP_TEST = $(TEST_SRC:%.test.c=$(DEP_DIR)/%.test.d)
ifeq ($(DEV),true)
	CFLAGS += -g3 -DDEV
	TEST_CFLAGS += -g3 -DDEV
endif

ifeq ($(CODE_COVERAGE),true)
	CFLAGS += -fprofile-arcs -ftest-coverage
	TEST_CFLAGS += -fprofile-arcs -ftest-coverage
	TEST_LDFLAGS += -fprofile-arcs -ftest-coverage
endif
MALLOC= malloc/libft_malloc.so
NAME = libfuzt.a
NAME_DYN = libfuzt.so
NAME_UP = FUZT
all: $(NAME) $(NAME_DYN)
test: $(TEST_BIN)
$(OBJ_DIR)/%.test.o: %.test.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(TEST_CFLAGS) -c $< -o $@
$(DEP_DIR)/%.test.d: %.test.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m"
	@$(CC) $(TEST_CFLAGS) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(OBJ_DIR)/%.o: %.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAGS) -c $< -o $@
$(DEP_DIR)/%.d: %.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m"
	@$(CC) $(CFLAGS) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(TEST_BIN): $(NAME) $(OBJ_TEST)
	@$(CC) $(TEST_CFLAGS) $(TEST_LDFLAGS) $(OBJ_TEST) $(NAME) -o $(TEST_BIN)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mTests done!\033[0m\n"
$(NAME): $(OBJ)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mLinking...\033[0m"
	@$(LN) $(LFLAGS) $(NAME) $(OBJ)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mDone!\033[0m\n"
$(NAME_DYN): $(OBJ)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mLinking...\033[0m"
	@$(CC) $(CFLAGS) -dynamiclib -o $(NAME_DYN) $(OBJ)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mDone!\033[0m\n"
install: $(NAME)
	mkdir -p /usr/local/include/fuzt
	mkdir -p /usr/local/lib
	cp include/* /usr/local/include/fuzt/
	cp $(NAME) /usr/local/lib
dclean:
	@rm -f $(DEP)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .d!\033[0m\n"
clean: dclean
	@rm -f $(OBJ) $(OBJ_TEST)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .o!\033[0m\n"
fclean: clean
	@rm -f $(NAME) $(TEST_BIN)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .a!\033[0m\n"
re:
	@$(MAKE) fclean
	@$(MAKE) all
-include $(DEP)
-include $(DEP_TEST)
.PHONY: all clean fclean re dclean test install
