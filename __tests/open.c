#include <stdlib.h>
#include <stdio.h>
#include "fuzt.h"
#include "setup.h"

int toto(const char *argv0)
{
	(void)open(argv0, O_RDONLY);
	return (0);
}

int main(int argc, const char **argv)
{
	(void)open(argv[0], O_RDONLY);
	if (open(argv[0], O_RDONLY) < 0)
		exit(10);
	if (open(argv[0], O_RDONLY) < 0)
		abort();
	(void)toto(argv[0]);
	if (open(argv[0], O_RDONLY) < 0)
		*((size_t*)1) = 1;
	(void)open(argv[0], O_RDONLY);
	return (0);
}