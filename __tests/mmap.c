#include <stdlib.h>
#include <stdio.h>
#include "fuzt.h"
#include "setup.h"
 #include <sys/mman.h>

int toto()
{
	(void)mmap(0, 4096, PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANON, -1, 0);
	return (0);
}

int main()
{
	(void)mmap(0, 4096, PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANON, -1, 0);
	if (!mmap(0, 4096, PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANON, -1, 0))
		exit(10);
	if (!mmap(0, 4096, PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANON, -1, 0))
		abort();
	(void)toto();
	size_t *toto = (size_t*)mmap(0, 4096, PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANON, -1, 0);
	*toto = 15;
	(void)mmap(0, 4096, PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANON, -1, 0);
	return (0);
}