#include <stdlib.h>
#include <stdio.h>
#include "fuzt.h"
#include "setup.h"

int toto()
{
	(void)write(1, "HelloWorld\n", 11);
	return (0);
}

int main()
{
	(void)write(1, "HelloWorld\n", 11);
	if (write(1, "HelloWorld\n", 11) < 0)
		exit(10);
	if (write(1, "HelloWorld\n", 11) < 0)
		abort();
	(void)toto();
	if (write(1, "HelloWorld\n", 11) < 0)
		*((size_t*)1) = 1;
	(void)write(1, "HelloWorld\n", 11);
	return (0);
}