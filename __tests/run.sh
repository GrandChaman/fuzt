#!/bin/bash

source ./__tests/utils.sh

make test

./fuzt_test

check_ret 0 $LINENO

./__tests/default_tests.sh malloc
./__tests/default_tests.sh realloc
./__tests/default_tests.sh calloc
./__tests/default_tests.sh open
./__tests/default_tests.sh mmap
./__tests/default_tests.sh write
./__tests/default_tests.sh fstat