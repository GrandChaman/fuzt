#include "fuzt.h"
#include <criterion/criterion.h>

Test(LList, CreateNewNode)
{
	t_fuzt_ref	*llist;

	llist = llist_new("Hello", "World", 1, "malloc");
	cr_expect(llist != NULL);
	cr_expect(llist->prev == NULL);
	cr_expect(llist->next == NULL);
	cr_expect(llist->bt != NULL);
	cr_expect(llist->bt_size != 0);
	cr_expect_str_eq(llist->file, "Hello");
	cr_expect_str_eq(llist->function, "World");
	cr_expect(llist->line == 1);
	llist_del(&llist);
	cr_expect(llist == NULL);
}

Test(LList, AppendNodeBack)
{
	t_fuzt_ref	*llist;

	llist = NULL;
	llist_push_back(&llist, llist_new("Hello", "World", 1, "malloc"));
	llist_push_back(&llist, llist_new("Hello", "World", 2, "malloc"));
	cr_expect(llist != NULL);
	cr_expect(llist->prev == NULL);
	cr_expect(llist->next != NULL);
	cr_expect(llist->line == 1);
	cr_expect(llist->next->line == 2);
	cr_expect(llist->next->prev == llist);
	cr_expect(llist->next->next == NULL);
	llist_destroy(&llist);
	cr_expect(llist == NULL);
}

Test(LList, AppendNodeFront)
{
	t_fuzt_ref	*llist;

	llist = NULL;
	cr_expect(llist_push_front(NULL, NULL) == NULL);
	llist_push_front(&llist, llist_new("Hello", "World", 1, "malloc"));
	llist_push_front(&llist, llist_new("Hello", "World", 2, "malloc"));
	cr_expect(llist != NULL);
	cr_expect(llist->prev == NULL);
	cr_expect(llist->next != NULL);
	cr_expect(llist->line == 2);
	cr_expect(llist->next->line == 1);
	cr_expect(llist->next->prev == llist);
	cr_expect(llist->next->next == NULL);
	llist_destroy(&llist);
	cr_expect(llist == NULL);
}

Test(LList, DeleteOne)
{
	t_fuzt_ref	*llist;

	llist = NULL;
	llist_push_back(&llist, llist_new("Hello", "World", 1, "malloc"));
	llist_push_back(&llist, llist_new("Hello", "World", 1, "malloc"));
	llist_del(&llist->next);
	cr_expect(llist != NULL);
	llist_destroy(&llist);
	cr_expect(llist == NULL);
}

Test(LList, Destroy)
{
	t_fuzt_ref	*llist;

	llist = NULL;
	llist_push_back(&llist, llist_new("Hello", "World", 1, "malloc"));
	llist_destroy(&llist);
	cr_expect(llist == NULL);
}
