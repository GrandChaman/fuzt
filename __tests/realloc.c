#include <stdlib.h>
#include <stdio.h>
#include "fuzt.h"
#include "setup.h"

int toto()
{
	(void)realloc(NULL, 0);
	return (0);
}

int main()
{
	(void)realloc(NULL, 1);
	if (!realloc(NULL, 10))
		exit(10);
	if (!realloc(NULL, 56))
		abort();
	(void)toto();
	size_t *toto = (size_t*)realloc(NULL, 64);
	*toto = 15;
	(void)realloc(NULL, 1);
	return (0);
}