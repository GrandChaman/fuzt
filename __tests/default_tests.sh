#!/bin/bash

source ./__tests/utils.sh

FUNCTION=$1

run_test_function_dynamic ()
{
	printf "Testing ${FUNCTION}_test (dynamic)"
	export FUZT_ENABLE=1 FUZT_DYNAMIC=1 
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 1 $LINENO
	if [ $(echo "${OUTPUT}" | grep  "=* FUZT =*" | wc -l)  != 4 ];
	then
		echo "Wrong number of banners"
		exit 1
	fi
	echo "${OUTPUT}" | grep  "Mode .* dynamic" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "Stop on call .* false" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "\[\d*\]\[+\d*\].*tests\/${FUNCTION}.c:\d*.*<.*>:.*${FUNCTION}:.*\(\d*\)" > /dev/null
	if [ $(echo "${OUTPUT}" | grep -e  "OK.*(10)" | wc -l)  != 2 ];
	then
		echo "Wrong number of OK with the right exit code"
		exit 2
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "OK.*(0)" | wc -l)  != 1 ];
	then
		echo "Wrong number of OK with the right exit code"
		exit 3
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "Abort.*trap.*(6)" | wc -l)  != 1 ];
	then
		echo "Wrong number of Abort"
		exit 4
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e "Segmentation.*fault.*(11)" | wc -l)  != 2 ];
	then
		echo "Wrong number of SEGVFAULT"
		exit 5
	fi
	check_ret 0 $LINENO
	unset FUZT_ENABLE FUZT_DYNAMIC 
	print_res 0
}

run_test_function_dynamic_stop_on_call ()
{
	printf "Testing ${FUNCTION}_test (dynamic) (stop on call)"
	export FUZT_ENABLE=1 FUZT_DYNAMIC=1 FUZT_STOP_ON_CALL=1 
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 1 $LINENO
	if [ $(echo "${OUTPUT}" | grep  "=* FUZT =*" | wc -l)  != 4 ];
	then
		echo "Wrong number of banners"
		exit 1
	fi
	echo "${OUTPUT}" | grep  "Mode .* dynamic" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "Stop on call .* true" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "\[\d*\]\[+\d*\].*tests\/${FUNCTION}.c:\d*.*<.*>:.*${FUNCTION}:.*\(\d*\)" > /dev/null
	if [ $(echo "${OUTPUT}" | grep -e  "OK.*(10)" | wc -l)  != 1 ];
	then
		echo "Wrong number of OK with the right exit code"
		exit 2
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "OK.*(0)" | wc -l)  != 3 ];
	then
		echo "Wrong number of OK with the right exit code"
		exit 3
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "Abort.*trap.*(6)" | wc -l)  != 1 ];
	then
		echo "Wrong number of Abort"
		exit 4
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "Segmentation.*fault.*(11)" | wc -l)  != 1 ];
	then
		echo "Wrong number of SEGVFAULT"
		exit 5
	fi
	check_ret 0 $LINENO
	unset FUZT_ENABLE FUZT_DYNAMIC FUZT_STOP_ON_CALL
	print_res 0
}


run_test_function_fixed ()
{
	printf "Testing ${FUNCTION}_test (fixed)"
	export FUZT_ENABLE=1 FUZT_FIXED=1
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 10 $LINENO
	if [ $(echo "${OUTPUT}" | grep  "=* FUZT =*" | wc -l)  != 2 ];
	then
		echo "Wrong number of banners"
		exit 1
	fi
	echo "${OUTPUT}" | grep  "Mode .* fixed" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "Stop on call .* false" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "\[\d*\]\[+\d*\].*tests\/${FUNCTION}.c:\d*.*<.*>:.*${FUNCTION}:.*\(\d*\)" > /dev/null
	export FUZT_ENABLE=1 FUZT_NB_CALL=0
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 10 $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=1
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 10 $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=2
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 134 # 128 + 6 (abort) $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=3
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 139 # 128 + 11 (SEGV) $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=4
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 139 # 128 + 11 (SEGV) $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=5
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 0 $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=1000
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 0 $LINENO
	export FUZT_ENABLE=1
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 0 $LINENO
	unset FUZT_ENABLE FUZT_NB_CALL  FUZT_FIXED
	print_res 0
}

run_test_function_fixed_stop_on_call ()
{
	printf "Testing ${FUNCTION}_test (fixed) (stop on call)"
	export FUZT_ENABLE=1 FUZT_STOP_ON_CALL=1 FUZT_FIXED=1
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep  "=* FUZT =*" | wc -l)  != 2 ];
	then
		echo "Wrong number of banners"
		exit 1
	fi
	echo "${OUTPUT}" | grep  "Mode .* fixed" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "Stop on call .* true" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "\[\d*\]\[+\d*\].*tests\/${FUNCTION}.c:\d*.*<.*>:.*${FUNCTION}:.*\(\d*\)" > /dev/null
	export FUZT_ENABLE=1 FUZT_NB_CALL=0
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 0 $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=1
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 10 $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=2
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 134 # 128 + 6 (abort) $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=3
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 0 # 128 + 11 (SEGV) $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=4
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 139 # 128 + 11 (SEGV) $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=5
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 0 $LINENO
	export FUZT_ENABLE=1 FUZT_NB_CALL=1000
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 0 $LINENO
	export FUZT_ENABLE=1
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 0 $LINENO
	unset FUZT_ENABLE FUZT_NB_CALL FUZT_STOP_ON_CALL FUZT_FIXED
	print_res 0
}

run_test_function_seq_replay ()
{
	printf "Testing ${FUNCTION}_test (seq_replay)"
	export FUZT_ENABLE=1 FUZT_SEQ_REPLAY=1 
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 1 $LINENO
	if [ $(echo "${OUTPUT}" | grep  "=* FUZT =*" | wc -l)  != 4 ];
	then
		echo "Wrong number of banners"
		exit 1
	fi
	echo "${OUTPUT}" | grep  "Mode .* fixed" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "Stop on call .* false" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "\[\d*\]\[+\d*\].*tests\/${FUNCTION}.c:\d*.*<.*>:.*${FUNCTION}:.*\(\d*\)" > /dev/null
	if [ $(echo "${OUTPUT}" | grep -e  "OK.*(10)" | wc -l)  != 2 ];
	then
		echo "Wrong number of OK with the right exit code"
		exit 2
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "OK.*(0)" | wc -l)  != 1 ];
	then
		echo "Wrong number of OK with the right exit code"
		exit 3
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "Abort.*trap.*(6)" | wc -l)  != 1 ];
	then
		echo "Wrong number of Abort"
		exit 4
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "Segmentation.*fault.*(11)" | wc -l)  != 2 ];
	then
		echo "Wrong number of SEGVFAULT"
		exit 5
	fi
	check_ret 0 $LINENO
	unset FUZT_ENABLE FUZT_SEQ_REPLAY
	print_res 0
}

run_test_function_seq_replay_stop_on_call ()
{
	printf "Testing ${FUNCTION}_test (seq_replay) (stop on call)"
	export FUZT_ENABLE=1 FUZT_SEQ_REPLAY=1  FUZT_STOP_ON_CALL=1 
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 1 $LINENO
	if [ $(echo "${OUTPUT}" | grep  "=* FUZT =*" | wc -l)  != 4 ];
	then
		echo "Wrong number of banners"
		exit 1
	fi
	echo "${OUTPUT}" | grep  "Mode .* fixed" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "Stop on call .* true" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "\[\d*\]\[+\d*\].*tests\/${FUNCTION}.c:\d*.*<.*>:.*${FUNCTION}:.*\(\d*\)" > /dev/null
	if [ $(echo "${OUTPUT}" | grep -e  "OK.*(10)" | wc -l)  != 1 ];
	then
		echo "Wrong number of OK with the right exit code"
		exit 2
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "OK.*(0)" | wc -l)  != 3 ];
	then
		echo "Wrong number of OK with the right exit code"
		exit 3
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "Abort.*trap.*(6)" | wc -l)  != 1 ];
	then
		echo "Wrong number of Abort"
		exit 4
	fi
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep -e  "Segmentation.*fault.*(11)" | wc -l)  != 1 ];
	then
		echo "Wrong number of SEGVFAULT"
		exit 5
	fi
	check_ret 0 $LINENO
	unset FUZT_ENABLE FUZT_SEQ_REPLAY FUZT_STOP_ON_CALL
	print_res 0
}

run_test_function_print_list ()
{
	printf "Testing ${FUNCTION}_test (print_list)"
	export FUZT_ENABLE=1 FUZT_PRINT_LIST=1
	OUTPUT=$(./__tests/${FUNCTION}_test)
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" | grep  "=* FUZT =*" | wc -l)  != 4 ];
	then
		echo "Wrong number of banners"
		exit 1
	fi
	echo "${OUTPUT}" | grep  "Mode .* fixed" > /dev/null
	check_ret 0 $LINENO
	echo "${OUTPUT}" | grep -e "Stop on call .* false" > /dev/null
	check_ret 0 $LINENO
	if [ $(echo "${OUTPUT}" |  grep -e "\[\d*\].*tests/${FUNCTION}.c:\d*.*<.*>:.*${FUNCTION}" | wc -l)  != 6 ];
	then
		echo "Wrong number of lines with the right exit code"
		exit 2
	fi
	check_ret 0 $LINENO
	unset FUZT_ENABLE FUZT_PRINT_LIST
	print_res 0
}

build		${FUNCTION}
run_test_function_dynamic
run_test_function_dynamic_stop_on_call
run_test_function_fixed
run_test_function_fixed_stop_on_call
run_test_function_seq_replay
run_test_function_seq_replay_stop_on_call
run_test_function_print_list
echo "==> With colors"
export FUZT_COLOR=1
run_test_function_dynamic
run_test_function_dynamic_stop_on_call
run_test_function_fixed
run_test_function_fixed_stop_on_call
run_test_function_seq_replay
run_test_function_seq_replay_stop_on_call
run_test_function_print_list