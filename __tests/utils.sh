#!/bin/bash
COL=$(tput cols)
RES_POS=$(($COL - 4))
MOVE_TO_COL="printf \\033[${RES_POS}G"
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)


print_res ()
{
	if [ $1 -eq 0 ]
        then
                $MOVE_TO_COL
                printf "%s" "${GREEN}[OK]${NORMAL}" $'\n'
        else
                $MOVE_TO_COL
                printf "%s" "${RED}[KO]${NORMAL}" $'\n'
                RES=1
        fi
}

check_ret ()
{
	RET=$?
	if [ $RET != $1 ];
	then
		print_res 1
		echo "FAILURE, return code is $RET and what expected to be $1 (Line $2)"
		exit 1;
	fi
}


build ()
{
	echo "Compiling $1_test"
	if [ $CODE_COVERAGE ];
	then
		clang "__tests/$1.c" -I include libfuzt.a -o "__tests/$1_test" -fprofile-arcs -ftest-coverage
	else
		clang "__tests/$1.c" -I include libfuzt.a -o "__tests/$1_test"
	fi
	echo "Compiled $1_test"
}
