#include <stdlib.h>
#include <stdio.h>
#include "fuzt.h"
#include "setup.h"

int toto()
{
	(void)malloc(0);
	return (0);
}

int main()
{
	(void)malloc(1);
	if (!malloc(10))
		exit(10);
	if (!malloc(56))
		abort();
	(void)toto();
	size_t *toto = (size_t*)malloc(64);
	*toto = 15;
	(void)malloc(1);
	return (0);
}