#include <stdlib.h>
#include <stdio.h>
#include "fuzt.h"
#include "setup.h"

struct stat buf;

int toto()
{
	(void)fstat(0, &buf);
	return (0);
}

int main()
{
	(void)fstat(0, &buf);
	if (fstat(0, &buf) < 0)
		exit(10);
	if (fstat(0, &buf) < 0)
		abort();
	(void)toto();
	if (fstat(0, &buf) < 0)
		*((size_t*)1) = 1;
	(void)fstat(0, &buf);
	return (0);
}