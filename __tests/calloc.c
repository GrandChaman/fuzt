#include <stdlib.h>
#include <stdio.h>
#include "fuzt.h"
#include "setup.h"

int toto()
{
	(void)calloc(10, 4964);
	return (0);
}

int main()
{
	(void)calloc(14, 371);
	if (!calloc(9, 1846))
		exit(10);
	if (!calloc(1, 1480))
		abort();
	(void)toto();
	size_t *toto = (size_t*)calloc(7, 2399);
	*toto = 15;
	(void)calloc(12, 2166);
	return (0);
}