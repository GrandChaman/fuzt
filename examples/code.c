#include <stdlib.h>
#include <fuzt/setup.h>

int main()
{
	(void)malloc(1);

	if ((write(1, "A simple message\n", 17)) < 0)
		abort();

	size_t *foo = (size_t*)malloc(sizeof(size_t));
	if (!foo)
		return (1);
	*foo = 0;
	free(foo);
	size_t *bar = (size_t*)malloc(sizeof(size_t));
	*bar = 6;
	return (0);
}