/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llist_push_front.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 17:09:37 by fle-roy           #+#    #+#             */
/*   Updated: 2019/10/04 18:12:45 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fuzt.h"

t_fuzt_ref	*llist_push_front(t_fuzt_ref **list, t_fuzt_ref *ref)
{
	t_fuzt_ref	*cursor;

	if (list == NULL || !ref)
		return (NULL);
	if (*list == NULL)
	{
		*list = ref;
		return (*list);
	}
	cursor = *list;
	while (cursor->prev)
		cursor = cursor->prev;
	cursor->prev = ref;
	ref->next = cursor;
	*list = ref;
	return (ref);
}
