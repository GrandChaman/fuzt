/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llist_destroy.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 13:21:52 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/30 14:10:37 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fuzt.h"

void		llist_destroy(t_fuzt_ref **list)
{
	t_fuzt_ref *next;

	if (!list || !*list)
		return ;
	while (*list)
	{
		next = (*list)->next;
		llist_del(list);
		*list = next;
	}
}
