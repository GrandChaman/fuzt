/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llist_del.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 17:29:58 by bluff             #+#    #+#             */
/*   Updated: 2019/10/07 18:36:13 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fuzt.h"

void	llist_del(t_fuzt_ref **alst)
{
	t_fuzt_ref	*save;
	
	if (!alst || !*alst)
		return ;
	save = *alst;
	if ((*alst)->next)
		(*alst)->next->prev = (*alst)->prev;
	if ((*alst)->prev)
		(*alst)->prev->next = (*alst)->next;
	free(save->bt);
	free(save);
	*alst = NULL;
}
