/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llist_push_back.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 17:09:32 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/30 14:13:13 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fuzt.h"

t_fuzt_ref	*llist_push_back(t_fuzt_ref **list, t_fuzt_ref *ref)
{
	t_fuzt_ref	*cursor;

	if (list == NULL)
		return (NULL);
	if (*list == NULL)
	{
		*list = ref;
		return (*list);
	}
	cursor = *list;
	while (cursor->next)
		cursor = cursor->next;
	cursor->next = ref;
	ref->prev = cursor;
	return (ref);
}
