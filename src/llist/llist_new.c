/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llist_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 17:08:48 by bluff             #+#    #+#             */
/*   Updated: 2019/10/04 15:56:31 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fuzt.h"
#include <execinfo.h>

t_fuzt_ref		*llist_new(const char *file, const char *function, size_t line,
	const char *name)
{
	t_fuzt_ref	*el;
	void		*ret_address[BACKTRACE_NUMBER];

	FAIL_ON_ERROR(el = malloc(sizeof(t_fuzt_ref)));
	el->bt_size = backtrace(ret_address, BACKTRACE_NUMBER);
	FAIL_ON_ERROR(el->bt = backtrace_symbols(ret_address,
		el->bt_size));
	el->res = -1;
	el->pid = -1;
	el->prev = NULL;
	el->next = NULL;
	el->file = file;
	el->name = name;
	el->function = function;
	el->line = line;
	return (el);
}
