#include "fuzt.h"
#include <dlfcn.h>
#include <string.h>

extern t_fuzt			g_fuzt;

void	futz_display_options(void)
{
	printf("%s %s %s\n", SEPARATOR_BAR, "FUZT", SEPARATOR_BAR);
	printf("Mode : %s\n", g_fuzt.dynamic ? "dynamic" : "fixed");
	if (!g_fuzt.dynamic)
		printf("Number of allowed call : %zu\n", g_fuzt.nb_call);
	printf("Stop on call : %s\n", g_fuzt.stop_on_call ? "true" : "false");
	printf("%s %s %s\n", SEPARATOR_BAR, "FUZT", SEPARATOR_BAR);
}

__attribute__((constructor))
void __before_main(int argc, const char **argv)
{
	const char *nb_call;
	char		enable;

	enable = 0;
	nb_call = NULL;
	enable = (getenv("FUZT_ENABLE") ? 1 : 0);
	nb_call = getenv("FUZT_NB_CALL");
	g_fuzt.dynamic = (getenv("FUZT_DYNAMIC") ? 1 : 0);
	if (nb_call)
		g_fuzt.nb_call = atoll(nb_call);
	g_fuzt.argc = argc;
	g_fuzt.argv = argv;
	g_fuzt.need_to_exit = 0;
	g_fuzt.fixed = (getenv("FUZT_FIXED") ? 1 : 0);
	g_fuzt.seq_replay = (getenv("FUZT_SEQ_REPLAY") ? 1 : 0);
	g_fuzt.print_list = (getenv("FUZT_PRINT_LIST") ? 1 : 0);
	g_fuzt.nb_call_backup = g_fuzt.nb_call;
	g_fuzt.color = (getenv("FUZT_COLOR") ? 1 : 0);
	g_fuzt.stop_on_call = (getenv("FUZT_STOP_ON_CALL") ? 1 : 0);
	if (enable)
		futz_display_options();
	g_fuzt.enable = enable;
}

int	futz_sequential_replay()
{
	t_fuzt_ref	*ref;
	size_t		i;
	int			ret;

	i = 0;
	ret = 0;
	ref = g_fuzt.ref;
	printf("%s %s %s\n", SEPARATOR_BAR, "FUZT", SEPARATOR_BAR);
	while (ref)
	{
		ret = fuzt_start_new_process(ref, i) || ret;
		ref = ref->next;
		i++;
	}
	printf("%s %s %s\n", SEPARATOR_BAR, "FUZT", SEPARATOR_BAR);
	return (ret);
}

int	futz_display_dynamic_result(void)
{
	t_fuzt_ref	*ref;
	size_t		i;
	int			ret;

	i = 0;
	ret = 0;
	ref = g_fuzt.ref;
	printf("%s %s %s\n", SEPARATOR_BAR, "FUZT", SEPARATOR_BAR);
	while (ref)
	{
		ret = fuzt_handle_process_return_value(ref, i) || ret;
		ref = ref->next;
		i++;
	}
	printf("%s %s %s\n", SEPARATOR_BAR, "FUZT", SEPARATOR_BAR);
	return (ret);
}

__attribute__((destructor))
void __after_main()
{
	int		ret;

	ret = 0;
	if (!g_fuzt.enable)
		return ;
	if (g_fuzt.dynamic)
		ret = futz_display_dynamic_result();
	else
	{
		if (g_fuzt.print_list)
			fuzt_print_list_call();
		if (g_fuzt.seq_replay)
			ret = futz_sequential_replay();
	}
	llist_destroy(&g_fuzt.ref);
	if (ret)
		exit(ret);
}

char	fuzt_routine(FUZT_LOCATION_PARAMS, const char *name)
{
	t_fuzt_ref	*ref;
	int			pid;

	if (!g_fuzt.enable)
		return (0);
	if (g_fuzt.need_to_exit)
		exit(0);
	ref = add_fuzt_ref(file, function, line, name);
	if (g_fuzt.dynamic)
	{
		fuzt_parallel_new_process(ref, &pid);
		if (!pid)
			return (1);
		g_fuzt.nb_call--;
		return (0);
	}
	if (g_fuzt.fixed && !g_fuzt.nb_call)
	{
		if (g_fuzt.stop_on_call)
			g_fuzt.need_to_exit = 1;
		return (1);
	}
	g_fuzt.nb_call--;
	return (0);
}

void	*fuzt_malloc(FUZT_LOCATION_PARAMS, size_t x)
	FUZT_FUNCTION(malloc, NULL, x)

void	*fuzt_realloc(FUZT_LOCATION_PARAMS, void *x, size_t y)
	FUZT_FUNCTION(realloc, NULL, x, y)

void	*fuzt_calloc(FUZT_LOCATION_PARAMS, size_t x, size_t y)
	FUZT_FUNCTION(calloc, NULL, x, y)

int		fuzt_open(FUZT_LOCATION_PARAMS, const char *x, int y)
	FUZT_FUNCTION(open, -1, x, y)

void	*fuzt_mmap(FUZT_LOCATION_PARAMS, void *addr, size_t len, int prot, int flags, int fd, off_t offset)
	FUZT_FUNCTION(mmap, NULL, addr, len, prot, flags, fd, offset)

int		fuzt_munmap(FUZT_LOCATION_PARAMS, void *addr, size_t len)
	FUZT_FUNCTION(munmap, -1, addr, len)

int		fuzt_write(FUZT_LOCATION_PARAMS, int fildes, const void *buf, size_t nbyte)
	FUZT_FUNCTION(write, -1, fildes, buf, nbyte)

int		fuzt_fstat(FUZT_LOCATION_PARAMS, int fildes, struct stat *buf)
	FUZT_FUNCTION(fstat, -1, fildes, buf)
