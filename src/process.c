#include "fuzt.h"
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <fcntl.h>
extern char **environ;
extern const char * const sys_siglist[];

void	fuzt_quiet_process()
{
	int fd;

	FAIL_ON_ERROR((fd = open("/dev/null", O_WRONLY)) >= 0)
	FAIL_ON_ERROR((dup2(fd, 1)) >= 0)
    FAIL_ON_ERROR((dup2(fd, 2)) >= 0)
    close(fd);
}

int		fuzt_handle_process_return_value(t_fuzt_ref *ref, size_t nb_call)
{
	const char	*str;
	int			ret;
	size_t		i;
	char		*color;

	
	str = "OK";
	ret = 0;
	i = -1;
	if (WIFSIGNALED(ref->res))
	{
		str = sys_siglist[WTERMSIG(ref->res)];
		ret = 1;
	}
	if (g_fuzt.color)
		color = ret ? COLOR_RED : COLOR_GRN;
	else
		color = "";
	printf("[%d][+%zu]\t%s:%zu\t<%s>:\t%s: %s%s%s ",
		ref->pid, nb_call, ref->file, ref->line, ref->function, ref->name,
		color, str, g_fuzt.color ? COLOR_RESET : "");
	printf("(%d)\n", ret ? WTERMSIG(ref->res) : WEXITSTATUS(ref->res));
	if (ret)
		while (++i < ref->bt_size)
			 if (i > (FUZT_BT_DEPTH - 1))
				printf("\t%s\n", ref->bt[i]);
	return (ret);
}

int		fuzt_start_new_process(t_fuzt_ref *ref, size_t nb_call)
{
	int		pid;
	int		stat_lock;
	char	nb_call_string[256];
	char	*str;
	int		ret;

	stat_lock = 0;
	str = "OK";
	ret = 0;
	FAIL_ON_ERROR((pid = fork()) >= 0);
	if (pid)
	{
		waitpid(pid, &stat_lock, 0);
		ref->res = stat_lock;
		ref->pid = pid;
		return (fuzt_handle_process_return_value(ref, nb_call));
	}
	fuzt_quiet_process();
	snprintf(nb_call_string, 256, "%zu", nb_call);
	setenv("FUZT_ENABLE", "true", 1);
	setenv("FUZT_FIXED", "true", 1);
	setenv("FUZT_NB_CALL", nb_call_string, 1);
	unsetenv("FUZT_SEQ_REPLAY");
	unsetenv("FUZT_PRINT_LIST");
	unsetenv("FUZT_DYNAMIC");
	return (execve(g_fuzt.argv[0], (char *const *)g_fuzt.argv, environ));
}

int		fuzt_parallel_new_process(t_fuzt_ref *ref, int *pid)
{
	int		stat_lock;
	char	*str;
	int		ret;

	stat_lock = 0;
	str = "OK";
	ret = 0;

	FAIL_ON_ERROR((*pid = fork()) >= 0);
	if (*pid)
	{
		waitpid(*pid, &stat_lock, 0);
		ref->res = stat_lock;
		ref->pid = *pid;
		return (0);
	}
	g_fuzt.dynamic = 0;
	g_fuzt.enable = 1;
	g_fuzt.fixed = 1;
	if (g_fuzt.stop_on_call)
		g_fuzt.need_to_exit = 1;
	else
		g_fuzt.nb_call = 0;
	fuzt_quiet_process();
	return (0);
}