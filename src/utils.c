#include "fuzt.h"

void		error_and_exit(int code)
{
	perror("fuzt");
	exit(code);
}

t_fuzt_ref	*add_fuzt_ref(const char *file, const char *function, size_t line,
	const char *name)
{
	t_fuzt_ref	*ref;

	FAIL_ON_ERROR(ref = llist_new(file, function, line, name));
	FAIL_ON_ERROR(llist_push_back(&g_fuzt.ref, ref));
	return (ref);
}

void		fuzt_print_list_call(void)
{
	t_fuzt_ref	*ref;
	size_t		i;

	i = 0;
	ref = g_fuzt.ref;
	printf("%s %s %s\n", SEPARATOR_BAR, "FUZT", SEPARATOR_BAR);
	while (ref)
	{
		printf("[%zu]\t%s:%zu\t<%s>:\t%s\n", i, ref->file, ref->line, ref->function,
			ref->name);
		ref = ref->next;
		i++;
	}
	printf("%s %s %s\n", SEPARATOR_BAR, "FUZT", SEPARATOR_BAR);
}