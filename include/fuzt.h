#ifndef FUZT_H
# define FUZT_H
# include <stdlib.h>
# include <stdio.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include <unistd.h>
# define DEV true
# define GOOD 0
# define BAD 1
# define BACKTRACE_NUMBER 10
# define SEPARATOR_BAR "========================"
# define COLOR_RESET  "\x1B[0m"
# define COLOR_RED  "\x1B[31m"
# define COLOR_GRN  "\x1B[32m"
# define COLOR_YEL  "\x1B[33m"
# define COLOR_BLU  "\x1B[34m"
# define COLOR_MAG  "\x1B[35m"
# define COLOR_CYN  "\x1B[36m"
# define COLOR_WHT  "\x1B[37m"
# define FUZT_BT_DEPTH  4
# define FAIL_ON_ERROR(x) \
	if (!(x)) \
		error_and_exit(-1);
# define FUZT_LOCATION_PARAMS \
	const char *file, const char *function, size_t line 
# define FUZT_FUNCTION(NAME, fail_ret, ...) \
	{ \
		if (fuzt_routine(file, function, line, #NAME)) \
			return (fail_ret); \
		return (NAME(__VA_ARGS__)); \
	}
	

typedef	struct s_fuzt		t_fuzt;
typedef	struct s_fuzt_ref	t_fuzt_ref;

typedef struct				s_fuzt_ref
{
	char					**bt;
	size_t					bt_size;
	int						res;
	int						pid;
	const char				*file;
	const char				*function;
	size_t					line;
	const char				*name;
	t_fuzt_ref				*prev;
	t_fuzt_ref				*next;
}							t_fuzt_ref;
typedef struct				s_fuzt
{
	size_t					nb_call_backup;
	size_t					nb_call;
	char					enable: 1;
	char					color: 1;
	char					need_to_exit: 1;
	char					dynamic: 1;
	char					stop_on_call: 1;
	char					seq_replay: 1;
	char					print_list: 1;
	char					fixed: 1;
	t_fuzt_ref				*ref;
	int						argc;
	const char				**argv;
	void					*(*malloc)(size_t);
}							t_fuzt;

t_fuzt						g_fuzt;

void						error_and_exit(int code);
t_fuzt_ref					*add_fuzt_ref(const char *file, const char *function, size_t line,
	const char *name);
void						fuzt_print_list_call(void);
char						fuzt_routine(FUZT_LOCATION_PARAMS, const char *name);


void						*fuzt_malloc(const char *file, const char *function,
	size_t line, size_t x);
void						*fuzt_realloc(const char *file, const char *function,
	size_t line, void *x, size_t y);
void						*fuzt_calloc(const char *file, const char *function,
	size_t line, size_t x, size_t y);

int							fuzt_open(FUZT_LOCATION_PARAMS, const char *x, int y);
void						*fuzt_mmap(FUZT_LOCATION_PARAMS, void *addr, size_t len,
	int prot, int flags, int fd, off_t offset);
int							fuzt_munmap(FUZT_LOCATION_PARAMS, void *addr,
	size_t len);
int							fuzt_write(FUZT_LOCATION_PARAMS, int fildes,
	const void *buf, size_t nbyte);
int							fuzt_fstat(FUZT_LOCATION_PARAMS, int fildes,
	struct stat *buf);

void						llist_del(t_fuzt_ref **alst);
void						llist_destroy(t_fuzt_ref **list);
t_fuzt_ref					*llist_new(const char *file, const char *function,
	size_t line, const char *name);
t_fuzt_ref					*llist_push_back(t_fuzt_ref **list, t_fuzt_ref *ref);
t_fuzt_ref					*llist_push_front(t_fuzt_ref **list, t_fuzt_ref *ref);

int							fuzt_start_new_process(t_fuzt_ref *ref, size_t nb_call);
int							fuzt_parallel_new_process(t_fuzt_ref *ref, int *pid);
int							fuzt_handle_process_return_value(t_fuzt_ref *ref,
	size_t nb_call);

#endif