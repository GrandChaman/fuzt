#ifndef FUZT_SETUP_H
# define FUZT_SETUP_H
# include "fuzt.h"
# define DEFINE_FUZT_FNC(name, ...) \
	fuzt_##name(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
# define malloc(...) DEFINE_FUZT_FNC(malloc, __VA_ARGS__)
# define realloc(...) DEFINE_FUZT_FNC(realloc, __VA_ARGS__)
# define calloc(...) DEFINE_FUZT_FNC(calloc, __VA_ARGS__)
# define open(...) DEFINE_FUZT_FNC(open, __VA_ARGS__)
# define mmap(...) DEFINE_FUZT_FNC(mmap, __VA_ARGS__)
# define munmap(...) DEFINE_FUZT_FNC(munmap, __VA_ARGS__)
# define write(...) DEFINE_FUZT_FNC(write, __VA_ARGS__)
# define fstat(...) DEFINE_FUZT_FNC(fstat, __VA_ARGS__)

#endif